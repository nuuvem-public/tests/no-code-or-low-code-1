# Test No Code / Low Code 1

The idea with this test is to allow us to better comprehend the skills of candidates to developer jobs, in various experience levels.

This test should be completed by you on your own, in your own time. Take as much time as you need, but usually this test shouldn't take more than a couple of hours to be done.

## Delivery instructions

Send an email to `apply@nuuvem.recruitee.com` with a URL to the deployed app and any instructions for us to test your app.

Feel free to add explanations about how you developed the app, any challenges faced during development, or to highlight some part of the app you think we shouldn't miss.

Please keep the app accessible after you submit the email, at least until we have a meeting to discuss the app with you.

## Project description

Using [Bubble](https://bubble.io/), design and implement a search engine interface that searches a database of Chuck Norris facts, powered by [ChuckNorris.io](https://api.chucknorris.io) API, and display the search results to the user.

The user must be able to do a full-text search and be presented with the matching jokes. When presenting the matching jokes to the user you should display the maximum amount of information that you can find about each joke, in a friendly manner to the user.

The design and usability should consider both desktop and mobile use cases (i.e. use a responsive design). Also, remember to cater to different pixel-density devices (with high DPI displays).

The user should be able to mark any joke as "favorite". The app should have a way to display all "favorite" jokes to the user. The user should alse be able to unmark any previously favorite joke so as to no longer be considered a "favorite". The user should be able to retrieve this favorite jokes from the app even after closing the browser and coming back to the app another day.

Bonus points:

- Can you implement a "I'm feeling lucky" feature, similar to Google's? Should it use a different result layout? Why (or why not)?
- Can you highlight the search criteria used on the search results? What are the pros and cons of that feature?

### Implementation

You should use [Bubble](https://bubble.io/) to develop this app. The free plan account should be sufficient. It should not require any plugin or custom Javascript code to develop this app as described above, but you are free to use them if you want. Use what you think is best for the task.

Your project MUST:

1. Be well documented
1. Have a consistent style and nomenclature in your Bubble project
1. Be mindful of performance

## Review

Your project will be evaluated by the following criteria:

1. Does the project fulfill the basic requirements?
1. Did you follow closely the project specification?
1. Quality of the user experience, in terms of usability, accessibility and overall visual presentation;
1. Quality of the Bubble project itself, how it's strutured and how it complies with good, modern practices;
1. Familiarity with the standard functionalities of Bubble;

## Reference

This test was inspired by this other test: <https://github.com/GuiaBolso/seja-um-guia-front>.
